%define SYS_READ 0
%define SYS_WRITE 1
%define SDTOUT 1
%define SDTIN 0
%define SYS_EXIT 60
%define STRING_END 0

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .lp:
    cmp  byte [rdi], STRING_END  ; break end of string
    je   .end
    inc  rdi            ; increment address
    jmp  .lp
  .end:
    sub  rdi, rax       ; calculate length (len = end_address - start_adress)
    mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rax, SYS_WRITE
    mov  rsi, rdi
    mov  rdi, SDTOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov rax, rdi
    	        xor rcx, rcx          ; counter
    	        mov r10, rsp          ; save rsp
    		sub rsp, 32
    		dec r10
    	        mov byte[r10], STRING_END       ; push end of string in stack
    	        mov rdi, 10
    	        
    	 .lp:   xor rdx, rdx
    	        div rdi
    	        inc rcx               ; increment counter
    	        add dl, '0'           ; make char of digit
    	        dec r10
    	        mov byte[r10], dl     ; save digit in stack
    	        cmp rax, 0
    	        je .ed
    	        jmp .lp
    	        
    	 .ed:   mov rdi, r10          ; Begining of string = rsp
    	        call print_string
    	        add rsp, 32          ; Return rsp value
    	        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .pos         ; if n>0 just call print_uint
    push rdi
    mov rdi, '-'
    call print_char  ; print '-' and call print_uint for positive number
    pop rdi
    neg rdi
   .pos: 
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .lp:   mov al, byte[rsi]    ; read char from string1
           cmp byte[rdi], al    ; cmp with char from string2
           jne .false           ; if false -> return 0
           cmp  byte [rdi], STRING_END
           je  .true            ; if STRING_END -> return 1
           inc rdi              ; increment string address pointers           
           inc rsi
           jmp .lp
    .false: mov rax, 0
           jmp .end
    .true:  mov rax, 1
    .end:   ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        push rax
        mov rdi, SDTIN
        mov rsi, rsp
        mov rdx, 1
        xor rax, rax
        syscall
        cmp rax, 0        ; if end of stream -> return 0
        jle  .no
        pop rax
        jmp .end
 .no:   pop rax
        xor rax, rax
 .end   ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        push rbx            ; store caller-saved registers in stack
        push r12
        push r13
        mov  r12, rdi       ; buffer address
        mov  r13, rsi       ; buffer size
        xor rbx, rbx        ; counter of string chars
 .spc:  call read_char      ; skip all 'space' chars in the beginning
        cmp al, ` `
        je  .spc
        cmp al, `\t`
        je  .spc
        cmp al, `\n`
        je  .spc
        cmp al, STRING_END
        je  .empty      
 .lp:   inc rbx             ; increment counter
        cmp al, ` `
        je  .nul            ; if space symbol -> return
        cmp al, `\t`
        je  .nul
        cmp al, `\n`
        je  .nul
        cmp al, STRING_END
        je  .nul
        cmp rbx, r13        ; if string size > buffer size -> fail
        je .fail
        mov byte[r12], al   ; write char into buffer
        inc r12             ; increment buffer address
        call read_char
        jmp .lp  
 .empty:xor rdx, rdx        ; if string is empty(contains zero chars)
        mov byte [r12], 0
        mov rax, r12
        jmp .end  
 .nul:  mov byte [r12], STRING_END  ; add STRING_END symbol into end of buffer
        mov rdx, rbx
        dec rdx
        mov rax, r12
        sub rax, rdx
        jmp .end   
 .fail: mov rax, 0 
 .end:  pop r13
        pop r12
        pop rbx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax         ; number
        xor rcx, rcx         ; counter
        mov r8, 10           ; multiplier
 .lp:   cmp byte[rdi], STRING_END
        je .end
        cmp byte[rdi], '0'   ; if char > '9' or char < '0' -> end parse
        jl .end
        cmp byte[rdi], '9'
        jg .end
        inc rcx              ; increment counter
        mul r8
        mov dl, byte[rdi]    ; read digit
        sub dl, '0'          ; char -> integer
        add al, dl           ; add digit to number 
        inc rdi              ; incremennt string address
        jmp .lp        
 .end:  mov rdx, rcx         ; return length of number
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        cmp byte[rdi], '-'
        je .minus          ; number is negative
        jmp parse_uint
 .minus:
        inc  rdi 
        call parse_uint
        cmp  rdx, 0     ; if fails -> return
        je .end
        neg  rax        ; read positive value and negate it
        inc  rdx        ; increment length by 1 (because of '-')
 .end:  ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax            ; counter
.lp:    cmp rax, rdx            ; if counter = buffer length -> fail
        je  .fail
        cmp byte[rdi], STRING_END    ; if end -> succ
        je  .succ
        inc rax                 ; increment counter
        mov cl, byte[rdi]       ; read char
        mov byte[rsi], cl       ; write char
        inc rsi                 ; increment adresses
        inc rdi
        jmp .lp
.fail:  xor rax, rax 
        jmp .end
.succ:  mov byte[rsi], STRING_END
.end:   ret
